/**
 * Finds a child element by its ID
 * @param {HTMLElement} root The root node to start the search at
 * @param {string} id The ID to search by
 */
function findElementById(root, id) {
    var traverse = function (root) {
        if (root.id == id) {
            return root;
        }
        for (var child = root.firstChild; child; child = child.nextSibling) {
            var found = traverse(child);
            if (found) {
                return found;
            }
        }
    };
    return traverse(root);
}


function performFetch(path, completion) {
    var xreq = new XMLHttpRequest();
    xreq.open('GET', path, true);
    xreq.send(null);
    xreq.onload = function () {
        completion(xreq.responseText);
    };
};

/**
 * Example component loader function -- asynchronously loads a component and invokes a callback when done
 * This is intended for demonstration use only. For a production application;
 * a server-side rendering framework that bundles everything together is recommended.
 * @param {string} componentName The name of the component
 * 
 */
function loadComponent(componentName, onComplete) {
    var xreq = new XMLHttpRequest();
    var pendingLoads = 2;
    var html;
    var completeLoad = function () {
        pendingLoads--;
        if (!pendingLoads) {
            onComplete(html);
        }
    };
    performFetch('components/' + componentName + '.js', function (data) {
        //Create script tag
        var scriptTag = document.createElement('script');
        scriptTag.innerText = data;
        document.head.appendChild(scriptTag);
        completeLoad();
    });
    performFetch('components/' + componentName + '.html', function (data) {
        html = data;
        completeLoad();
    });
}