function createClock(app, clockDiv) {
    var time = findElementById(clockDiv, 'time');
    var widget = app.createWidget(clockDiv);
    setInterval(function () {
        time.innerText = Date();
    }, 1000);
    time.innerText = Date();
    return widget;
}