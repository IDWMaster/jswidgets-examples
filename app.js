function createDemoApplication(app) {
    var domNode = document.createElement('div');
    var demoApp = app.createWidget(domNode);

    var grid = app.createGrid();
    grid.addFullRow().addAutoRow().addFullRow();
    grid.addFullColumn().addAutoColumn().addFullColumn();

    var domNode = app.root.getDomNode();
    domNode.innerHTML = '';
    app.root.addWidget(demoApp);
    demoApp.addWidget(grid);
    //Load the clock component asynchronously.
    loadComponent('clock', function (html) {
        var clockDiv = document.createElement('div');
        clockDiv.innerHTML = html;
        grid.addWidget(createClock(app, clockDiv.cloneNode(true)), 0, 0);
        grid.addWidget(createClock(app, clockDiv.cloneNode(true)), 1, 1); //Add a centered clock widget.
        grid.layout();
    });
    return demoApp;
}